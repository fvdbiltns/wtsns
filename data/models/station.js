var mongoose = require('mongoose');

module.exports = mongoose.model('Station', {
    afkorting: { type: String, default: "" },
    naam: { type: String, default: "" }
}); 