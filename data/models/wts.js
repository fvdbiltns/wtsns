var mongoose = require('mongoose');

module.exports = mongoose.model('Wts', {
    station:{ type: String, default: "" },
    spoor: { type: Number, default: 0 },
    deel_spoor: { type: String, default: "" },
    richting: { type: String, default: "" },
    vlicht_aantal: { type: Number, default: -1 },
    vlicht_dbz: { type: Boolean, default: false },
    sein_aanwezig: { type: String, enum: ['ja', 'nee', 'invoer'], default: 'nee' },
    seinen: [{type: String, default: ""}] ,
    boog: { type: Boolean, default: false },
    mdw_aantal: { type: Number, default: -1},
    wijzigingsDatum: { type: String, default: "" }
}); 