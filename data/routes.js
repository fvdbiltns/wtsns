var Wts = require("./models/wts");
var Station = require("./models/station");
var mongoose = require('mongoose');

function today() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear() +"";

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm; 
    }

    return dd + '/' + mm + '/' + yyyy.substr(yyyy.length-2);

}


module.exports = function (app) {

    // get / read stations all

    app.get("/api/stations", function (req, res) {
        Station.find(function (err, station) {
            if (err) {
                res.send(err);

            } else {
                res.json(station);

            }
        });
    });

    // post / create station 

    app.post("/api/station", function (req, res) {
        var station = new Station();

        station.afkorting = req.body.afkorting;
        station.naam = req.body.naam;
        station.wts = req.body.wts;

        station.save(function (err, response) {
            if (err) {
                res.send(err);
            } else {
                res.json({ message: 'Station added', data: station });
            }
        })
    });

    // delete alle gegevens / deleteall

    app.delete('/api/stations/delete', function (req, res) {
        Station.remove({}, function (err) {
            console.log('Station collection removed with error ' + err);
            if (err == null) {
                Wts.remove({}, function (err) {
                    console.log('WTS collection removed with error ' + err);
                    res.json({ message: "WTS & stations removed wiithout error" });
                });
            }
        });
    });

    //get / wts read wts for station

    app.get("/api/station/wts/:naam", function (req, res) {
        Wts.find({station: req.params.naam}, function (err, wts) {
            if (err) {
                res.send(err);
            } else {
                res.json({ message: 'Got WTS with id ' + req.params.wts_id, data: wts });

            }
        })
    });

    // post / create wts 

    app.post("/api/wts", function (req, res) {
        var wts = new Wts();

        wts.station = req.body.station;
        wts.spoor = req.body.spoor;
        wts.deel_spoor = req.body.deel_spoor;
        wts.richting = req.body.richting;
        wts.vlicht_aantal = req.body.vlicht_aantal;
        wts.vlicht_dbz = req.body.vlicht_dbz;
        wts.sein_aanwezig = req.body.sein_aanwezig;
        wts.seinen = req.body.seinen;
        wts.boog = req.body.boog;
        wts.mdw_aantal = req.body.mdw_aantal;
        wts.wijzigingsDatum = req.body.wijzigingsDatum;

        wts.save(function (err, response) {
            if (err) {
                res.send(err);
            } else {
                res.json({ message: 'WTS added', data: wts });
            }
        });
    });

    // update wts 

    app.put("/api/wts", function (req, res) {
        Wts.findOne({ _id: req.body._id }, function (err, wts) {
            if (err) {
                res.send(err);
            }if (wts == null){
                wts = new Wts(); 
            }
                if (wts != null) {
                wts.station = req.body.station;
                wts.spoor = req.body.spoor;
                wts.deel_spoor = req.body.deel_spoor;
                wts.richting = req.body.richting;
                wts.vlicht_aantal = req.body.vlicht_aantal;
                wts.vlicht_dbz = req.body.vlicht_dbz;
                wts.sein_aanwezig = req.body.sein_aanwezig;
                wts.seinen = req.body.seinen;
                wts.boog = req.body.boog;
                wts.mdw_aantal = req.body.mdw_aantal;
                wts.wijzigingsDatum = today();

                wts.save(function (err, response) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.json({ message: 'WTS added', data: wts });
                    }
                });

            }
        }
        );
    });

    //delete specific wts 

    app.delete('/api/wts/delete/:wts_id', function (req, res) {
        Wts.findByIdAndRemove(req.params.wts_id, function (err, wts) {
            if (err) {
                res.send(err);
            } else {
                res.json({ message : "wts removed"});
            }
        });
    });

    //anders afhankelijk van appRoutes 

    app.get('*', function (req, res) {
        res.sendfile('./web/views/index.html');
    });

};