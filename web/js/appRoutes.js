angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // stationslijst is ook home page
        .when('/', {
            templateUrl: 'views/station.html',
            controller: 'StationController'
        })

        .when('/wts', {
            templateUrl: 'views/station.html',
            controller: 'StationController'
        })
        
        .when('/wts/:id', {
            templateUrl: 'views/update.html',
            controller: 'StationUpdateController'
        });

    $locationProvider.html5Mode(true);

}]);
