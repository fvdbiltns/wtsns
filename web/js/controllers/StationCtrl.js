angular.module('StationCtrl', ['StationService', 'WtsService']).controller('StationController', function ($scope, StationService, WtsService, $http) {

    $scope.stationList = [];


    $scope.loading = false;

    $scope.created = false;

    $scope.getStations = function () {
        if (!$scope.loading) {
            console.log("get stations");
            $scope.stationList = [];
            $scope.aantal = 0;
            $scope.created = false; 
            StationService.get().then(function (response) {
                var stations = response.data;
                if (angular.isArray(stations) && stations.length > 270) {
                    $scope.stationList = $scope.convertStations(stations);
                } else {
                    $scope.stationList = [];
                }
                console.log("stations: " + $scope.stationList.length + ", stations in db: " + stations.length);
                $scope.isLoading();
            });
        }

    };

    $scope.convertStations = function (stations) {
        var converted = [];
        stations.forEach(s => {
            var station = {
                afkorting: s.afkorting,
                naam: s.naam,
                displayName: s.afkorting + " - " + s.naam,
                wijzigingsDatum: s.wijzigingsDatum,
                wts: [],
                showWts: false
            };
            $scope.convertWts(station);
            converted.push(station);

        });
        return converted;
    }

    $scope.aantal = 0;

    $scope.convertWts = function (s) {
        var converted = [];
        WtsService.getWtsForStation(s.naam).then(function (response) {

            wts = response.data.data;

            wts.forEach(w => {
                var convert = {
                    station: s.afkorting,
                    spoornr: w.spoor,
                    deel_spoor: w.deel_spoor,
                    spoor: $scope.spoor_txt(w),
                    richting: w.richting,
                    vlicht: $scope.vlicht_txt(w),
                    sein: $scope.sein_text(w),
                    mdw: $scope.mdw_txt(w.mdw_aantal),
                    boog: $scope.ja_nee(w.boog)
                }
                s.wijzigingsDatum = w.wijzigingsDatum;
                s.wts.push(convert);
                $scope.aantal++;
            });
        });

    }

    $scope.spoor_txt = function (w) {
        var spoor = "" + w.spoor;
        if (angular.isDefined(w.deel_spoor)) {
            spoor += w.deel_spoor;
        }
        return spoor;
    }

    $scope.mdw_txt = function (mdw_aantal) {
        if (mdw_aantal >= 0) {
            var mdw = "Ja ";
            if (mdw_aantal > 0) {
                mdw += mdw_aantal + "x";
            }
            return mdw;
        }
        return "Nee"
    }

    $scope.vlicht_txt = function (w) {
        var vlicht = "";
        if (w.vlicht_aantal >= 0) {
            vlicht = "Ja ";
            if (w.vlicht_aantal > 0) {
                vlicht += w.vlicht_aantal + "x";
            }
            if (w.vlicht_dbz) {
                vlicht += " dbz";
            }
        } else {
            vlicht = "Nee";
        }
        return vlicht;
    }

    $scope.ja_nee = function (bool) {
        if (bool) {
            return "Ja";
        }
        return "Nee";
    }

    $scope.startImport = function (files) {
        if (confirm("Weet je zeker dat je een nieuw bestand wilt importeren? Alle WTS gegevens gaan daarmee verloren. ")) {
            $scope.handleFile(files[0]);
        }
    };

    $scope.getStations();

    $scope.hasSeinen = function (seinen) {
        var hasSeinen = false;
        if (angular.isArray(seinen) && seinen.length > 0) {
            seinen.forEach(s => {
                if (s !== "") {
                    hasSeinen = true;
                }
            })
        }
        return hasSeinen;
    }

    $scope.sein_text = function (wts) {
        if (!angular.isDefined(wts.sein_aanwezig) && !$scope.hasSeinen(wts.seinen)) {
            console.log("wts has no sign: " + JSON.stringify(wts) + " on spoor " + $scope.spoor_txt(wts));
        } else if (wts.sein_aanwezig.toLowerCase() == 'nee' || !$scope.hasSeinen(wts.seinen)) {
            return "Nee";
        } else if (wts.sein_aanwezig.toLowerCase() == 'ja') {
            return "Ja"
        } else {
            var zichtbare_seinen = "";
            for (var i = 0; i < wts.seinen.length; i++) {
                if (i > 0 &&
                    wts.seinen[i] !== "" &&
                    zichtbare_seinen !== "") {
                    zichtbare_seinen += ", ";
                }
                zichtbare_seinen += wts.seinen[i];
            }
            return zichtbare_seinen;
        }
    }

    $scope.isLoading = function () {
        $scope.loading = $http.pendingRequests.length !== 0;

        if ($scope.loading == false && $scope.created ==true){
            $scope.getStations(); 
        }
        return $scope.loading;

    }


    $scope.handleFile = function (file) {

        if (file) {

            var stations = [];

            var reader = new FileReader();

            reader.onload = function (e) {

                $scope.isLoading();

                var data = e.target.result;

                var workbook = XLSX.read(data, { type: 'binary' });

                var first_sheet_name = workbook.SheetNames[0];

                var dataObjects = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name], { raw: true });
                if (dataObjects.length > 0) {
                    $scope.totaal = dataObjects.length;

                    StationService.deleteAll().then(function (response) {
                        dataObjects.forEach(function (row) {
                            var station = {};
                            station.afkorting = row.Afkorting;
                            station.naam = row.Naam;

                            var inList = false;
                            for (var i = 0; i < stations.length; i++) {
                                if (stations[i].afkorting == station.afkorting) {
                                    station = stations[i];
                                    inList = true;
                                    break;
                                }
                            }
                            if (!inList) {
                                StationService.create(station);
                                stations.push(station);
                            }

                            var wts = {};

                            wts.station = station.naam;
                            wts.spoor = $scope.split_spoor(row.Spoor)[0];
                            wts.deel_spoor = $scope.split_spoor(row.Spoor)[1];
                            wts.richting = row.Richting;
                            wts.vlicht_aantal = parseInt($scope.split_value(row.Vertreklicht)[1]);
                            wts.vlicht_dbz = $scope.split_value(row.Vertreklicht)[3];
                            wts.sein_aanwezig = $scope.split_value(row.Sein)[4];
                            var sein1 = $scope.split_value(row.Sein)[0];
                            var sein2 = $scope.split_value(row.Sein)[1];
                            if (sein2 < 0) {
                                sein2 = "";
                            }
                            wts.seinen = [sein1, sein2];
                            wts.boog = $scope.split_value(row.Boog)[2];
                            wts.mdw_aantal = $scope.split_value(row.Medewerkerskast)[1];

                            if ($scope.isNumber(row.Gewijzigd)) {
                                wts.wijzigingsDatum = $scope.convertRawDate(row.Gewijzigd);
                            } else {
                                wts.wijzigingsDatum = row.Gewijzigd;
                            }



                            WtsService.create(wts).then(function (data) {
                                $scope.getStations();

                                $scope.created = true;
                            });
                        }, this);

                    });

                } else {
                    console.log('Something went wrong');
                    $scope.msg = "Error : Something Wrong !";
                }

            }

            reader.onerror = function (ex) {
                console.log('Something went wrong with exception');
            }

            reader.readAsBinaryString(file);
        }

    }

    $scope.showWts = function (station) {
        station.showWts = !station.showWts;
    }


    $scope.zipHTM = function () {
        downloadHTML($scope.convertToHTM(), 'wts-cl-sp.zip');
    }

    $scope.zipRailpocket = function () {
        downloadHTML($scope.convertToHTMs(), 'wts-railpocket.zip');
    }

    $scope.zipORP = function () {
        downloadCSV($scope.convertToCSV(), 'wts-orp.zip');
    }

    $scope.zipExcel = function () {
        downloadExcel($scope.convertToExcel(), 'wts-excel.zip');
    }

    $scope.zipAll = function () {
        downloadAll($scope.convertToHTM(), $scope.convertToHTMs(), $scope.convertToCSV(), $scope.convertToExcel(), 'wts-all.zip');
    }

    $scope.isNumber = function (o) {
        return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
    }

    $scope.split_spoor = function (value) {

        var values = [];
        values[0] = "";// spoornummer
        values[1] = "";//spoortoevoeging
        if ($scope.isNumber(value)) {
            values[0] = parseInt(value);

        }
        else {
            values[1] = value.substr(value.length - 1);
            values[0] = parseInt(value.replace(values[1], ""))

        }
        return values
    }

    $scope.split_value = function (value) {

        var values = [];
        values[0] = ""; //ingevuld sein
        values[1] = -1; // aantallen of extra sein value
        values[2] = false; // ja of nee
        values[3] = false;// wel of niet dubbelzijdig
        values[4] = "nee"; // invoeren geselecteerd


        if (angular.isDefined(value) &&
            value != "" &&
            value != null) {
            v = value;
        } else {
            return values;
        }
        if ($scope.isNumber(v)) {
            v += "";

        }
        try {
            if (v.toLowerCase().indexOf("ja") >= 0) {
                values[2] = true;
                values[4] = "ja";
                values[1] = 0;
                console.log("ja");
                if (v.toLowerCase().indexOf("dbz") >= 0) {
                    values[3] = true;
                }
                try {
                    var aantal = parseInt(v.toLowerCase().replace("ja", "").replace("x", "").replace("dbz", ""));
                    if ($scope.isNumber(aantal)) {
                        values[1] = aantal;
                    }
                } catch (err) {
                    console.log("Cant parse value cause of " + err);
                }
            }
            else if (v.toLowerCase().indexOf("nee") >= 0 || v.indexOf("-") >= 0) {
                values[2] = false;
                values[4] = "nee";
                values[1] = -2;
            }
            else {
                values[4] = "invoer";
                splits = v.split("+");

                for (var x = 0; x < splits.length; x++) {
                    values[x] = splits[x] + "";
                }
            }

        } catch (err) {
            console.log("error splitting " + JSON.stringify(v));
        }

        return values;

    }

    $scope.convertRawDate = function (rawdate) {
        var date = new Date(1900, 0, 0);
        // var date = new Date(pre_date.getUTCFullYear(), pre_date.getUTCMonth(), pre_date.getUTCDay());
        // console.log(pre_date + " to " + date); 
        date.setDate(date.getDate() + rawdate);

        var dd = date.getUTCDate();
        var mm = date.getUTCMonth() + 1; //January is 0!
        var yyyy = date.getUTCFullYear() + "";

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }
        //return pre_date + " to " + date; 
        return dd + '/' + mm + '/' + yyyy.substr(yyyy.length - 2);
    }


    $scope.convertToHTMs = function () {
        var firstlines = '<html>\n<head>\n<title>WTS</title>\n<link rel="stylesheet" type="text/css" href="rp-template-css.css">\n</head>\n<body>\n<div>\n<h1>';
        var htms = new Array();
        var htmContent = "";
        var htmDocs = 0;

        $scope.stationList.forEach(station => {
            htms[htmDocs] = new Array();
            htmContent += firstlines + station.afkorting + " " + station.naam + "  </h1>\n<table width ='240' border='1' cellspacing='0' cellpadding='2'>\n<tr>";
            htmContent += "<td align = 'center'> Sp </td>";
            htmContent += "<td align = 'center'> Richting </td>";
            htmContent += "<td align = 'center'> Vlicht </td>";
            htmContent += "<td align = 'center'> Sein </td>";
            htmContent += "<td align = 'center'> Boog </td>";
            htmContent += "<td align = 'center'> mdkast </td></tr>";
            htms[htmDocs][0] = station.displayName + ".htm";
            station.wts.forEach(wts => {
                htmContent += '\n<td align = "center"> ' + wts.spoor + '</td>';
                htmContent += '\n<td align = "center"> ' + wts.richting + ' </td>';
                htmContent += '\n<td align = "center"> ' + wts.vlicht + ' </td>';
                htmContent += '\n<td align = "center">  ' + wts.sein + ' </td>';
                htmContent += '\n<td align = "center">  ' + wts.boog + '</td>';
                htmContent += '\n<td align = "center">  ' + wts.mdw + ' </td> </tr> ';
            });
            htmContent += '</table>Deze informatie is gewijzigd op ' + station.wijzigingsDatum.replace("/", "-");

            htms[htmDocs][1] = htmContent;

            if (htmDocs == 0) {
                console.log(htmContent);
            }

            htmContent = "";
            htmDocs++;
        });
        return htms;
        //  downloadHTML(htms, 'WTS.zip') ;   
    }

    $scope.convertToHTM = function () {
        var lines = "<!DOCTYPE html><html><head><title>WTS</title><meta charset='UTF-8'><style>body {font:20px arial, sans-serif;}div {margin-left: 5px;}a {text-decoration: none;}table.index {border-collapse: collapse;}table.index td {border: 5px solid #ffffff;}table.index td {background-color: #ffd633;}table.index a {color: #000000; font:bold 60px arial, sans-serif;}table.index td {text-align: center;}table.station {border-collapse: collapse;}table.station td, th {border: 1px solid #000000;}table.station th, td {text-align: center; padding: 8px;}table.station th {color: #ffffff; background-color: #1a8cff;}h1 {color: #1a8cff;}</style></head><body><h1>WTS per station</h1><h2>Kies de beginletter van het gewenste station.</h2><table class='index'>";
        for (var i = 0; i < 5; i++) {
            lines += "<col width=\"90px\" />"
        }
        var contents = [];
        var letters = [];

        for (var i = 0; i < $scope.stationList.length; i++) {
            var station = $scope.stationList[i];
            var htmContent = "<h1"
            if (angular.isDefined(station.afkorting) &&
                station.afkorting != null &&
                letters.indexOf(station.afkorting.substr(0, 1)) < 0) {
                var letter = station.afkorting.substr(0, 1)
                letters.push(letter);
                htmContent += " id='" + letter + "'";
            }
            htmContent += ">" + station.displayName + "</h1>";
            htmContent += '<p> </p><table class="station"><col width="100px" /><col width="100px" /><col width="150px" /><col width="100px" /><col width="100px" /><col width="100px" /><col width="150px" /><tr><th>Spoor</th><th>Richting</th><th>Vertreklicht</th><th>Sein</th><th>Boog</th><th>Mdw kast</th><th>Gewijzigd</th></tr>';
            station.wts.forEach(wts => {
                htmContent += '\n<tr><td> ' + wts.spoor + '</td>';
                htmContent += '\n<td> ' + wts.richting + ' </td>';
                htmContent += '\n<td> ' + wts.vlicht + ' </td>';
                htmContent += '\n<td>  ' + wts.sein + ' </td>';
                htmContent += '\n<td>  ' + wts.boog + '</td>';
                htmContent += '\n<td>  ' + wts.mdw + '</td>';
                htmContent += '\n<td>  ' + station.wijzigingsDatum + ' </td> </tr> ';
            });
            htmContent += '</table><a href="#" >&#8679; naar boven</a>';
            contents.push(htmContent);
        }
        var htms = new Array();

        htms[0] = new Array();

        for (var x = 0; x < letters.length; x++) {
            var letter = letters[x];
            if (x % 5 == 0) {
                if (x > 0) {
                    lines += "</tr>";
                }
                lines += "<tr>";
            }
            lines += '<td><a href = "#' + letter + '" class="button">' + letter + '</a></td>';
        }
        lines += '</tr></table><p> </p>';
        contents.forEach(content => {
            lines += "\n" + content;
        });

        lines += "</div></body></html>";

        htms[0][0] = "wtslijst.htm";

        htms[0][1] = lines;

        return htms;
    }

    $scope.convertToExcel = function () {
        var data = [['Afkorting', 'Naam', 'Spoor', 'Richting', 'Vertreklicht', 'Sein', 'Boog', 'Medewerkerskast', 'Gewijzigd']]

        $scope.stationList.forEach(station => {
            station.wts.forEach(wts => {
                data.push([station.afkorting, station.naam, wts.spoor, wts.richting, wts.vlicht, wts.sein, wts.boog, wts.mdw, station.wijzigingsDatum]);
            });
        });
        return data;
        //  downloadExcel(data, 'WTS.zip'); 
    }

    $scope.convertToCSV = function () {

        var table = '"Station;Spoor;Richting;Vlicht;Sein;Boog;mdwkast;Wijzigingsdatum;Opmerkingen" \n'
        $scope.stationList.forEach(station => {
            station.wts.forEach(wts => {
                table += station.afkorting + ';';
                table += wts.spoor + ';';
                table += wts.richting + ';';
                table += wts.vlicht + ';';
                table += wts.sein + ';';
                table += wts.boog + ';';
                table += wts.mdw + ';';
                table += station.wijzigingsDatum + '; \n';
            });
        });

        return table;
        // downloadCSV(table, 'WTS.zip'); 
    }

    $scope.sorterFunc = function (person) {
        return parseInt(person.id);
    };




});