angular.module('StationUpdateController', ['StationService', 'WtsService']).controller('StationUpdateController', function ($scope, StationService, WtsService, $http, $routeParams, $location) {

    $scope.wts = [];
    $scope.station = "";
    $scope.number = -1;
    $scope.updated = false; 

    $scope.loadStation = function () {
        WtsService.getWtsForStation($routeParams.id).then(function (response) {
            $scope.wts = response.data.data;
            if ($scope.wts.length > 0) {
                $scope.station = $scope.wts[0].station;

            }

        });

    }

    $scope.loadStation();

    $scope.deleteRowConfirmation = function (wts) {
        if (confirm("Weet je zeker dat je deze wts wilt verwijderen? Dit kan niet ongedaan worden.")) {
            $scope.delete(wts);
        }
    }

    $scope.delete = function (wts) {
        console.log(wts);
        WtsService.delete(wts._id).then(function (response) {

            $scope.loadStation();
        });
    }

    $scope.isLoading = function () {

        var loading = $http.pendingRequests.length !== 0;
        if (!loading && $scope.updated) {
            $location.url('/wts');
        }
        return loading;

    }

    $scope.saveWts = function () {
        $scope.wts.forEach(w => {
            w.station = $scope.station; 
            WtsService.update(w).then(function (response) {
                $scope.updated = true; 
                $scope.loadStation();
            });
        });
    }

    $scope.addRow = function () {
        var w = {};
        $scope.wts.push(w);
    }


}).filter('range', function () {
    return function (input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i = min; i <= max; i++)
            input.push(i);
        return input;
    };
});; 
