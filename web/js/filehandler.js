/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
; (function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend() {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[i];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init(converter) {
		function api(key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) { }

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				return (document.cookie = [
					key, '=', value,
					attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
					attributes.path ? '; path=' + attributes.path : '',
					attributes.domain ? '; domain=' + attributes.domain : '',
					attributes.secure ? '; secure' : ''
				].join(''));
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) { }
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) { }
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () { });
}));



function handleCsv(fileToRead) {
	var reader = new FileReader();
	// Read file into memory as UTF-8      
	reader.readAsText(fileToRead);
	// Handle errors load
	reader.onload = loadHandler;
	reader.onerror = errorHandler;
	document.getElementById("content").innerHTML = "";
}

function loadHandler(event) {
	var csv = event.target.result;
	convertToHTM(csv);
}

function processData(csv) {
	var allTextLines = csv.split(/\r\n|\n/);
	var lines = "";
	for (var i = 0; i < allTextLines.length; i++) {
		var data = allTextLines[i].split(';');
		var tarr = "";
		for (var j = 0; j < data.length; j++) {

			tarr += data[j] + " ";
		}
		lines += tarr + "<br>";
	}
	document.getElementById("content").innerHTML = lines + "done";
	download(lines, 'blabla.zip');
}

function errorHandler(evt) {
	if (evt.target.error.name == "NotReadableError") {
		alert("Canno't read file !");
	}
}

function downloaden(text, name, type) {

	var file = new Blob([text], { type: type });
	var xhr = new XMLHttpRequest();
	var fd = new FormData();

	xhr.open("GET", name, true);
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && xhr.status == 200) {
			console.log(xhr.responseText);
			alert("send");
		} else {

			alert("readystate: " + xhr.readyState + ", status: " + xhr.status);
		}
	}
	fd.append("upload_file", file);
	xhr.send(fd);

}

function downloadHTML(docs, zipname) {

	var zip = new JSZip();
	for (var i = 0; i < docs.length; i++) {
		zip.file(docs[i][0], docs[i][1]);
	}

	zip.generateAsync({ type: "blob" })
		.then(function (blob) {
			saveAs(blob, zipname);
		//	prepareDownloadBlobLink(blob, zipname);
		}, function (err) {
			alert("Download failed.");
		});
}


function downloadAll(htm, docs, csv, data, zipname) {
	var zip = new JSZip();
	var wb = new Workbook(), ws = sheet_from_array_of_arrays(data);
	wb.SheetNames.push(ws_name);
	wb.Sheets[ws_name] = ws;
	var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
	var myBlob = new Blob([s2ab(wbout)],{type:"application/octet-stream"});

	zip.file("Excel/wtsregels.xlsx", myBlob);
	zip.file('ORP/WTS.CSV', csv);
	for (var i = 0; i < htm.length; i++) {
		zip.file("CL-SP/" + htm[i][0], htm[i][1]);
	}
	for (var i = 0; i < docs.length; i++) {
		zip.file("Railpocket/" + docs[i][0], docs[i][1]);
	}
	zip.generateAsync({ type: "blob" })
		.then(function (blob) {
			saveAs(blob, zipname);
		//	prepareDownloadBlobLink(blob, zipname);
		}, function (err) {
			alert("Download failed.");
		});
}

function downloadCSV(doc, zipname) {
	var zip = new JSZip();
	
	zip.file('WTS.CSV', doc);
	console.log (JSON.stringify(zip));
	zip.generateAsync({ type: "blob" })
		.then(function (blob) {
			saveAs(blob, zipname);
		//	prepareDownloadBlobLink(blob, zipname);
		}, function (err) {
			alert("Download failed.");
		});
}

function downloadExcel(data, zipname) {

	var zip = new JSZip();
	var wb = new Workbook();
	var ws = sheet_from_array_of_arrays(data, {'date_format':'mm/dd/yyyy'});
	wb.SheetNames.push(ws_name);
	wb.Sheets[ws_name] = ws;
	var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
	var myBlob = new Blob([s2ab(wbout)],{type:"application/octet-stream"});


	zip.file("wtsregels.xlsx", myBlob);
	zip.generateAsync({ type: "blob" })
		.then(function (blob) {
			saveAs(blob, zipname);
	//		prepareDownloadBlobLink(blob, zipname);
		}, function (err) {
			alert("Download failed.");
		});
}

function prepareDownloadBlobLink(blob, zipname) {
	var a = document.getElementById("a");
	var url = window.URL.createObjectURL(blob);
	a.download = zipname;
	a.href = url;
}

function datenum(v, date1904) {
	if (date1904) v += 1462;
	var epoch = Date.parse(v);
	return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}

function sheet_from_array_of_arrays(data, opts) {
	var ws = {};
	var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
	for (var R = 0; R != data.length; ++R) {
		for (var C = 0; C != data[R].length; ++C) {
			if (range.s.r > R) range.s.r = R;
			if (range.s.c > C) range.s.c = C;
			if (range.e.r < R) range.e.r = R;
			if (range.e.c < C) range.e.c = C;
			var cell = { v: data[R][C] };
			if (cell.v == null) continue;
			var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

			if (typeof cell.v === 'number') cell.t = 'n';
			else if (typeof cell.v === 'boolean') cell.t = 'b';
			else if (cell.v instanceof Date) {
				cell.t = 'n'; cell.z = XLSX.SSF._table[14];
				cell.v = datenum(cell.v);
			}
			else cell.t = 's';

			ws[cell_ref] = cell;
		}
	}
	if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	return ws;
}

/* original data */
var ws_name = "wtsregels";

function Workbook() {
	if (!(this instanceof Workbook)) return new Workbook();
	this.SheetNames = [];
	this.Sheets = {};
}



function s2ab(s) {

	var buf = new ArrayBuffer(s.length);
	var view = new Uint8Array(buf);
	for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
	return buf;
}
/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
; (function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend() {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[i];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init(converter) {
		function api(key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) { }

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				return (document.cookie = [
					key, '=', value,
					attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
					attributes.path ? '; path=' + attributes.path : '',
					attributes.domain ? '; domain=' + attributes.domain : '',
					attributes.secure ? '; secure' : ''
				].join(''));
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) { }
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) { }
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () { });
}));

function changetekst() {
	document.getElementById("uploadFile").value = wad.value;
};

function put_text(id, place) {
	var text = document.getElementById(id).value;
	document.getElementById(place).innerHTML = text;
	if (place == 'tag') {
		document.getElementById('tag2').innerHTML = text;
	}
}

function validate() {
	if (window.FileReader) {
		var max = document.getElementById('input-max').value;
		var file = document.getElementById('uploadFile').value;
		var tag = document.getElementById('input-tag').value;
		var filename = wad.value.split(/(\\|\/)/g).pop();
		if (tag == "" || max == "" || file == "") {
			alert('Voer alle invoervelden in');
		} else if (filename == 'stvz.wad') {
			handleWadfileToRead(wad.files[0]);
		} else {
			alert(filename + ' is niet het juiste bestand. Selecteer het bestand stvz.wad.');
		}
	} else {
		alert('FileReader are not supported in this browser.');
	}
}
function handleWadfileToRead(fileToRead) {
	var reader = new FileReader();
	// Read file into memory as UTF-8      
	reader.readAsText(fileToRead);

	// Handle errors load
	reader.onload = loadWadHandler;
	reader.onerror = errorHandler;
	document.getElementById("content").innerHTML = "";
}

function loadWadHandler(event) {
	var wad = event.target.result;
	var dirtyXML = wad.split("<?xml")[0];
	var newxml = wad.replace(dirtyXML, "").replace("Packed wat", "");
	//document.getElementById("content").innerHTML ="<textarea style='width:500px; height: 500px; '>" + newxml + "</textarea>"
	var xmllist = newxml.split('<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>');
	var max = document.getElementById('input-max').value;
	var tag = document.getElementById('input-tag').value;

	parseXMLFromText(tag, xmllist, max);
	//document.getElementById("content").innerHTML =  "<textarea style='width:500px; height: 500px; '>" + newxml + "</textarea>" ;
}

function errorHandler(evt) {
	if (evt.target.error.name == "NotReadableError") {
		alert("Canno't read file !");
	}
}



