angular.module('StationService', []).factory('StationService', ['$http', function($http) {

    return {
        get : function() {
            return $http.get('/api/stations');
        },

        create : function(stationData) {
            return $http.post('/api/station', stationData);
        },

        deleteAll : function() {
            return $http.delete('/api/stations/delete');
        }
    }       

}]);
