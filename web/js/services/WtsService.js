angular.module('WtsService', []).factory('WtsService', ['$http', function($http) {

    return {
        getWtsForStation : function(station) {
            return $http.get('/api/station/wts/' + station);
        },

        create : function(wtsData) {
            return $http.post('/api/wts/', wtsData);
        },
        
        update : function(wtsData) {
            return $http.put('/api/wts',  wtsData);
        },

        delete : function(id) {
            return $http.delete('/api/wts/delete/' + id);
        }
    }       

}]);
